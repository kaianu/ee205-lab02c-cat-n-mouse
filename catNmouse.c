///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02c - Cat `n Mouse - EE 205 - Spr 2022
///
/// This is a classic "I'm thinking of a number" guessing game.  The mouse
/// will think of a number... and the cat will keep trying to guess it.
///
/// @file    catNmouse.c
/// @version 1.0 - Initial version
///
/// Compile: $ gcc -o catNmouse catNmouse.c
///
/// Usage:  catNmouse [n]
///   n:  The maximum number used in the guessing game
///
/// Exit Status:
///   1:  The command line parameter was not a valid value
///   0:  The cat finally guessed correctly
///
/// Example:
///   $ ./catNmouse 2000
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1385
///   No cat... the number I’m thinking of is smaller than 1385
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1382
///   No cat... the number I’m thinking of is larger than 1382
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1384
///   You got me.
///   |\---/|
///   | o_o |
///    \_^_/
///   
/// @author  Kaianu Reyes-Huynh <kaianu@hawaii.edu>
/// @date    01/26/2022
///////////////////////////////////////////////////////////////////////////////

#define DEFAULT_MAX_NUMBER 2048

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main( int argc, char* argv[] ) {
   printf( "Cat `n Mouse\n" );
   //printf( "The number of arguments is: %d\n", argc );

   int n = atoi( argv[1] ) ;

   if (n < 1){
      printf("You must enter a number that's greater or equal to 1\n");
      exit(1);
   }

   if (n > DEFAULT_MAX_NUMBER){
      printf("Max number is 2048, run program again.\n");
      exit(1);
   }
   
   int aGuess;
   srand(time(0));
   int randNumber;

   randNumber = rand() % n;
   // printf("%d\n", randNumber);

   printf("OK cat, I'm thinking of a number from 1 to %d.\tMake a guess:\t", n);

   // This is an example of getting a number from a user
   // Note:  Don't enter unexpected values like 'blob' or 1.5.  
   scanf( "%d", &aGuess );

   while (aGuess != randNumber){

      if (aGuess > randNumber){
         printf("No cat... the number I'm thinking of is smaller than %d.\n", aGuess);
      }
      if (aGuess < randNumber){
         printf("No cat... the number I'm thinking of is larger than %d.\n", aGuess);
      }

      scanf( "%d", &aGuess);
   }
   
   printf("You got me.\n");
   printf("|\\---/|\n| o_o |\n \\_^_/\n");

   return 0;  // This is an example of how to return a 1
}

